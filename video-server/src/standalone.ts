import * as http from 'http';
import * as BodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';
import * as path from 'path';
import hash from 'object-hash';
import sharp from 'sharp';
import * as fs from "fs";
import { spawn, exec } from 'child_process';

/**
 * 
 *  Create directory : 
 *      mkdir -p /home/pi/VIDEO_SERVER/SavedFrames
 * 
 */

const savedFramesPath = "/home/pi/VIDEO_SERVER/SavedFrames"
const videoShotPath = "/home/pi/VIDEO_SERVER/VideoShot.jpg"

const framesDirectory = path.resolve(savedFramesPath) + '/';
type Crop = {left: number, top: number, width: number, height: number, threshold: number, negate: boolean};
type cropParam = false | Crop; 


export class FrameGrabber {

 
    constructor() {
        this.launchRetrieve();
    }

    public async get(crop: cropParam = false) {
        let buff = fs.readFileSync(videoShotPath);
        if (crop && crop.left) {
            buff = await sharp(buff).extract(crop).toBuffer();
        }
        if(crop && crop.threshold && crop.threshold > 0){
            buff = await sharp(buff).threshold(crop.threshold).toBuffer();
        }
        if(crop && crop.negate){
            buff = await sharp(buff).negate().toBuffer();
        }
        return buff
    }

    private launchRetrieve() {
        const source = spawn('/usr/bin/ffmpeg', 
        ["-y", "-f", "v4l2", "-i", "/dev/video0","-r","3","-update","1","-f","image2",videoShotPath],
        {stdio: ['ignore', 'ignore', 'ignore']})
        
        process.on('exit', () => {
            source.kill();
        })
    }

}

export class OCR {

    read(buffer: Buffer) {
        return new Promise((resolve, reject) => {
            const source = exec('tesseract -l fra stdin -', (err:any, stdout:any) => {
                if(err){
                    reject(err);
                }else{
                    resolve(stdout);
                }
            })
            source.stdin!.write(buffer);
            source.stdin!.end();
        });
    }
 
}

export class VideoServer {

    port: number;
    app: express.Express;
    server: http.Server;

    frameGrabber = new FrameGrabber();
    ocrReader = new OCR();
    
    constructor(port: number){
        this.port = port; 
        this.app = express();
        this.app.use(BodyParser.json());
        this.app.use(cors());
        this.server = http.createServer(this.app);
        this.app.get('/screen', (req, res) => this.screen(req, res))
        this.app.get('/ocr', (req, res) => this.ocr(req, res))
        this.app.get('/saveSub', (req, res) => this.saveSub(req, res))
        this.app.get('/validSub', (req, res) => this.validSub(req, res))
    }

    private readQueryParams(req: express.Request){
        return {
            left: 1* (req.query.left as any),
            top: 1* (req.query.top as any),
            width: 1* (req.query.width as any),
            height: 1* (req.query.height as any),
            threshold: 1* (req.query.threshold as any),
            negate: (req.query.negate as any) === 'true'
        };
    }
 
    private getFrameForQuery(req: express.Request){
        const crop: Crop|false = req.query.left ? this.readQueryParams(req) : false;
        return this.frameGrabber.get(crop)
    }

    // http://192.168.0.45:3615/screen?left=150&top=100&width=400&height=100
    async screen(req: express.Request, res: express.Response){
        this.getFrameForQuery(req).then( (buff) => {
            res.end(buff)
        }, (err) => {
            res.end(err);
        });
    }
     
    // http://192.168.0.45:3615/ocr?left=150&top=100&width=400&height=100
    async ocr(req: express.Request, res: express.Response){
        this.getFrameForQuery(req).then( (buff) => {
            this.ocrReader.read(buff).then( (text) => {
                res.json({text})
            }, (err) => {
                res.end(err);
            })
        }, (err) => {
            res.end(err);
        });
    }

    async saveSub(req: express.Request, res: express.Response){
        const frame = await this.getFrameForQuery(req);
        const params = this.readQueryParams(req);
        const objectToSave = {...params, ...{frame: frame.toString('hex')}}
        const objectHash = hash(objectToSave);
        const filepath = framesDirectory + objectHash;
        fs.writeFileSync(filepath, JSON.stringify(objectToSave), {mode: 0o777, encoding: 'utf8' });
        res.json({objectHash});
    }

    async validSub(req: express.Request, res: express.Response){
        const filepath = framesDirectory + req.query.hash;
        if(!fs.existsSync(filepath)){
            res.json({isSameFrame: false, notFound: true});
            return;
        }
        const params = JSON.parse(fs.readFileSync(filepath, 'utf8'));
        const currentFrame = await this.frameGrabber.get(params)
        const isSameFrame = currentFrame.toString('hex') === params.frame;
        res.json({isSameFrame});
    }
 
    public listen(){
        this.server.listen(this.port, () => {
            console.log(`Server started on port ${this.port} :)`);
        });
    }


}
