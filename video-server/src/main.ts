import { VideoServer } from "./standalone";

export function main() {
    const server = new VideoServer(8080);
    server.listen();
}

main();
