const DELAY = 100; 
const http = require('http');
class HTTP {
    public static getVideo(url: string){
      return new Promise((resolve, reject) => {
        http.get('http://192.168.0.55:8080' + url, function(res){
            var body = '';
            res.on('data', function(chunk){
                body += chunk;
            });
            res.on('end', function(){
                resolve(JSON.parse(body));
            });
        }).on('error', reject);
      })
    }
    public static postLog(url: string; body: string){
      new Promise((resolve, reject) => {
      const req = http.request({
        host: '192.168.0.55',
        port: '8091',
        path: url,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(body)
        }
      }, function(res){
          var body = '';
          res.on('data', function(chunk){
              body += chunk;
          });
          res.on('end', function(){
              resolve(body);
          });
      }); 
      req.write(body); 
      req.end();
      });
    }
    
}
class VIDEO {
public static async getText(
    left:number, 
    top: number, 
    width: number, 
    height: number, 
    threshold: number = 0,
    negate: boolean = false
){
    return (await HTTP.getVideo('/ocr' + this.params({
    left, top, width, height, threshold, negate
    }))).text;
}
private static async isSubFrame(hash: string){
        return (await HTTP.getVideo('/validSub' + this.params({
        hash
        }))).isSameFrame; 
}
private static params(json: any){
    return '?' + 
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}
}
class LOG {
public static async log(text: string){
    console.log(text);
}
}
class SWITCH {
  static async pairPad(){
    await pad.LT.press(); 
    await pad.RT.press(); 
    await delay(1000); 
    await pad.LT.release(); 
    await pad.RT.release(); 
    await delay(1000); 
    await pad.A.pressAndRelease();
    await delay(1000); 
  }
  static async pairPadAndBack2Game(){
    await SWITCH.pairPad(); 
    await delay(1500);
    await pad.B.pressAndRelease();
    await delay(1500);
    await pad.leftStick.setY(0);
    await delay(100); 
    await pad.leftStick.reset();
    await pad.leftStick.setX(0);
    await delay(500);
    await pad.leftStick.reset();
    await pad.A.pressAndRelease();
  }
  static async wake(){
    await pad.HOME.press(); 
    await delay(5000); 
    await pad.HOME.release();
    await delay(1000); 
    await pad.HOME.press(); 
    await delay(5000); 
    await pad.HOME.release();
    await delay(5000); 
    
  }  
  static async demo(){
      await this.pairPad();
      await Promise.all([
        pad.leftStick.loop(),
        pad.rightStick.loop(false)
      ]);
      (async function(){
          await Promise.all([
            pad.leftStick.loop(true, 1000, 5),
            pad.rightStick.loop(false, 1000, 5)
          ]);     
      })();
      await pad.dpad.setPosition(0,1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(1,1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(1,0);
      await defaultButtonDelay();
      await pad.dpad.setPosition(1,-1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(0,-1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(-1,-1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(-1,0);
      await defaultButtonDelay();
      await pad.dpad.setPosition(-1,1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(0,1);
      await defaultButtonDelay();
      await pad.dpad.setPosition(0,0);
      await defaultButtonDelay();
      await  pad.A.pressAndRelease(); //
      await pad.MINUS.press();
      for(var i = 0; i < 20; i++){
        await  pad.X.pressAndRelease(); //
      }
      await pad.MINUS.release(); 
      await pad.PLUS.press();
      for(var i = 0; i < 20; i++){
        await  pad.HOME.pressAndRelease(); //
      }
      await  pad.Y.pressAndRelease(); //
      await  pad.ZRT.pressAndRelease(); //
      await  pad.PLUS.release();
      await  pad.LT.pressAndRelease(); //   
  }
  static async resetGame(){
    await pad.HOME.pressAndRelease();
    await delay(3000);
    await pad.X.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(10000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(10000);
    await pad.A.pressAndRelease();
    await delay(5000);
  }
  static async isConnexionServeurInterrompue(){
    const textToFind = 'interrompue';
    return (await VIDEO.getText(500,500,1000,55, 160, true)).indexOf(textToFind) !== -1; 
  }
}
class PKMN {
  // Fly 
  static async chooseFlyDest(x: number, y: number){
    await pad.dpad.setX(-1);
    await delay(4000);
    await pad.dpad.setX(0);
    await delay(500);
    await pad.dpad.setY(-1);
    await delay(5000);
    await pad.dpad.setY(0);
    await delay(500);

     await pad.dpad.setY(1);
     await delay(100*y);
     await pad.dpad.setY(0);
     await delay(500);

     await pad.dpad.setX(1);
     await delay(100*x);
     await pad.dpad.setX(0);
     await delay(500);
    
  }
  static async isFlyDestContaining(text: string){
    const isTaxiVolantAvailable = (await VIDEO.getText(880,1035,160,40,160,true)).indexOf('Taxi volant') !== -1;
    if(!isTaxiVolantAvailable){ return false }
    const destinationText = await VIDEO.getText(1300,180,400,120,160,true);
    return destinationText.indexOf(text) !== -1
  }
  // Breed 
  static async flyBackToPension(){
    await pad.X.pressAndRelease(); 
    await delay(2000); 
    await pad.dpad.setX(-1);
    await delay(3000);
    await pad.dpad.setX(0);
    await pad.dpad.setY(-1);
    await delay(3000);
    await pad.dpad.setY(0);
    await pad.dpad.setX(1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(600); 
    await pad.dpad.setX(1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(1000);    
    do{
      await pad.X.pressAndRelease(); 
      await delay(500);
      await pad.leftStick.setDegree(90);
      await delay(40);
      await pad.leftStick.setDegree(0);
      await delay(40);
      await pad.leftStick.center(); 
      await delay(500); 
    } while (
      !(await PKMN.isFlyDestContaining('Garderie'))  
    )
    await delay(500); 
    for(let i = 0; i < 10; i++){
        await pad.A.pressAndRelease();
    }
    await delay(3000); 
  }
  static async isOeufAskingForEclosion(){
    const textToFind = 'Hein';
    return (await VIDEO.getText(400, 890,210,55, 160, true)).indexOf(textToFind) !== -1; 
  }
  static async playOeufEclosion(){
    await pad.leftStick.center();
    for(let i = 0; i < 5; i++){
      await pad.A.pressAndRelease();  
    }
    await delay(20000);
    for(let i = 0; i < 5; i++){
      await pad.B.pressAndRelease();  
    }
    await delay(1000);
    for(let i = 0; i < 5; i++){
      await pad.B.pressAndRelease();  
    }
    await delay(2000); 
    await pad.B.pressAndRelease();
    await delay(3000);
  }
  static async pensionWalkUntilOeufIsAskingForEclosion(){
    // From pension door
    const timeOneWay = 3600;
    while(true){
      await pad.leftStick.setX(255)
      await delay(timeOneWay);
      await pad.leftStick.setX(0);
      await delay(timeOneWay);
      await pad.leftStick.setX(0);
      await delay(timeOneWay);     
      await pad.leftStick.setX(255);
      await delay(timeOneWay + 50);
      await pad.leftStick.center();
      if(await PKMN.isOeufAskingForEclosion()){
        break;
      }
    }
  }
  static async pensionGirlIsProposingEgg(){
    const textToFind = 'on Pokémon tenait un';
    return (await VIDEO.getText(400, 870,800, 180, 140, false)).indexOf(textToFind) !== -1; 
  }
  static async goSpeakToPensionGirl(){
    // From pension door
    await pad.leftStick.setDegree(-90);
    await delay(700);
    await pad.leftStick.setDegree(0);
    await delay(1000);
    await pad.leftStick.setDegree(90);
    await delay(100);
    await pad.leftStick.center();
  }
  static async speakToPensionGirlAndExchangePkmnAtPos(position: number = 6){
    await pad.A.pressAndRelease();
    await delay(3000);
    if(!(await PKMN.pensionGirlIsProposingEgg())){
      for(let i=0; i<9; i++){ await pad.B.pressAndRelease(); await delay(900); } 
      return false;
    }
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);    
    // Selecting last pokemon 
    const direction = position > 0 ? 1 : -1;
    if(direction < 0){ position--; }
    for(let i = 1; i < Math.abs(position); i++){
        await pad.dpad.setY(direction);
        await delay(100);
        await pad.dpad.setY(0);
        await delay(1000);
    }
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.B.pressAndRelease();
    await delay(3000);
    return true;
  }
  static async speakToPensionGirlAndRecuperePoke(){
    await pad.A.pressAndRelease();
    await delay(3000);
    while((await PKMN.pensionGirlIsProposingEgg())){
      for(let i=0; i<9; i++){ await pad.B.pressAndRelease(); }; 
      await pad.A.pressAndRelease();
      await delay(3000);
    }
    await pad.A.pressAndRelease();
    await delay(3000);    
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.dpad.setY(1);
    await delay(100);
    await pad.dpad.setY(0);
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(3000);    
    for(let i=0; i<9; i++){ await pad.B.pressAndRelease(); await delay(900) }; 
  }
  static async speakToPensionGirlAndGivePoke(boite: number, position: number){
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await PKMN.goBoiteXPosX(boite, position);
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(3000);  
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);
    await pad.A.pressAndRelease();
    await delay(3000);    
    for(let i=0; i<5; i++){ await pad.B.pressAndRelease(); await delay(900) }; 
  }
  static async flyBackToPensionGirl(){
    await PKMN.flyBackToPension();
    await delay(1000);
    await PKMN.goSpeakToPensionGirl();
    await delay(1000);
  }
  static async doOneBaby(withStats: boolean = true){
    await PKMN.flyBackToPension();
    await pad.leftStick.setDegree(180); 
    await delay(800); 
    await pad.leftStick.center();
    await PKMN.pensionWalkUntilOeufIsAskingForEclosion();
    await PKMN.playOeufEclosion();
    if(withStats){
      const stats = await PKMN.getStatsOfPokeAtPos();
      LOG.log({context: ' BABIES ', stats});
    }
    await PKMN.flyBackToPension();
    await PKMN.goSpeakToPensionGirl();
    while(!(await PKMN.speakToPensionGirlAndExchangePkmnAtPos(-1))){
      await PKMN.flyBackToPension();
      await pad.leftStick.setDegree(180); 
      await delay(800); 
      await pad.leftStick.center();
      const timeOneWay = 3600;
      for(let i = 0; i < 3; i++){
        await pad.leftStick.setX(255)
        await delay(timeOneWay);
        await pad.leftStick.setX(0);
        await delay(timeOneWay);
        await pad.leftStick.setX(0);
        await delay(timeOneWay);     
        await pad.leftStick.setX(255);
        await delay(timeOneWay + 50);
        await pad.leftStick.center();
      }      
      await PKMN.flyBackToPension();
      await PKMN.goSpeakToPensionGirl();           
    }
    await delay(1000);
  }
  static async autoDoBabies(maxCount:number = -1, withStats: boolean = true){
    let count = 0;
    while(maxCount <= 0 || count < maxCount){
      await PKMN.doOneBaby(withStats);
      console.log('baby', (count+1), 'over', maxCount);
      count++;
    }
  }
  // Online 
  static async isCommYLocal(){
    const textToFind = 'local';
    return (await VIDEO.getText(1600, 10, 280, 60, 160, true)).indexOf(textToFind) !== -1;   
  }
  static async isCommYInternet(){
    const textToFind = 'Internet';
    return (await VIDEO.getText(1600, 10, 280, 60, 160, true)).indexOf(textToFind) !== -1; 
  }
  static async isInternetActivePopupPresent(){
    const textToFind = 'Connexion à Internet activée';
    return (await VIDEO.getText(535, 885,510,50, 160, true)).indexOf(textToFind) !== -1; 
  }
  static async openMagicTradeMenu(){
    await pad.Y.pressAndRelease(); 
    await delay(3000)
    if(await PKMN.isCommYLocal()){
      await pad.PLUS.pressAndRelease(); 
      await delay(2000);
      while(!(await PKMN.isInternetActivePopupPresent())){
        await delay(1000);
      }
      await pad.A.pressAndRelease();
      await delay(2000);  
    }
    await pad.dpad.setY(1);
    await delay(100);
    await pad.dpad.setY(0);
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(3000);
  }
  static async isMagicTradeComplete(){
    // left=150&top=1005&width=300&height=60
    const textToFind = 'change effectu';
    return (await VIDEO.getText(150,1005,300,60,190,true)).indexOf(textToFind) !== -1;
  }
  static async tradePokemonForMagicTrade(){
    for(let i = 0; i < 15; i++){
      await pad.A.pressAndRelease(); 
      await delay(2000);  
    }
  }
  static async receivePokemonFromMagicTrade(){
    await pad.Y.pressAndRelease(); 
    await delay(3000); 
    for(let i = 0; i < 60; i++){
      await pad.A.pressAndRelease(); 
      await delay(1000);
      if(await PKMN.canDoCommY()){
        await delay(1000);
        return;
      }
    }

  }
  static async canDoCommY(){
    return (
      (await VIDEO.isSubFrame('f1bb54af0c566ed1d3bdd8bbd5052ca6aebdb068')) ||
      (await VIDEO.isSubFrame('0772afeff020df43f0b3c105ba81d11b4b5bd609'))
    );
  }
  static async autoMagicTrade(
     startBoite: number; 
     startPos: number; 
     endBoite: number;
     endPos: number;
     showReceivedPokeStats: boolean = false
    ){
    let currentBoite = startBoite; 
    let currentPos = startPos;
    let maxScoreAccepted = endBoite*30 + endPos;
    while(true){
      const currentScore = currentBoite*30 + currentPos;
      if(currentScore > maxScoreAccepted) {
        return;
      }
      await PKMN.openMagicTradeMenu()
      await PKMN.goBoiteXPosX(currentBoite,currentPos);
      const orignalStats = await PKMN.getStatistics();
      if(orignalStats === null){
        currentPos++; 
        if(currentPos > 30){
          currentPos = 1;
          currentBoite++;
        }
        continue;
      }      
      if(orignalStats.isInteressant){
        LOG.log({context: ' -KEEP- ', orignalStats, currentBoite,currentPos});
      } else {
        await PKMN.tradePokemonForMagicTrade();
        while(!(await PKMN.isMagicTradeComplete())){
          await delay(1000);
          if(await SWITCH.isConnexionServeurInterrompue()){
            for(let i=0;i<9;i++){ await pad.B.pressAndRelease(); await delay(900); }
              if(await PKMN.isCommYLocal()){
                await pad.PLUS.pressAndRelease(); 
                await delay(2000);
                while(!(await PKMN.isInternetActivePopupPresent())){
                  await delay(1000);
                }
                await pad.A.pressAndRelease();
                await delay(2000);  
              }
              for(let i=0;i<9;i++){ await pad.B.pressAndRelease(); await delay(900); }
          }
        }
        await PKMN.receivePokemonFromMagicTrade();
        if(showReceivedPokeStats){
          const receivedStats = await PKMN.getStatsOfPoke(currentBoite,currentPos);
          LOG.log({context: ' TRADES ', receivedStats, currentBoite,currentPos});
        }
      }
      currentPos++; 
      if(currentPos > 30){
        currentPos = 1;
        currentBoite++;
      }
    }
  }
  

  // Boites 
  static async getCurrentBoiteNumber(){
    await pad.dpad.setY(1);
    await delay(200);
    await pad.dpad.setY(0);
    await delay(100);
    const boiteText = await VIDEO.getText(735, 100,380,65, 120, false));
    await pad.dpad.setY(-1);
    await delay(200);
    await pad.dpad.setY(0);
    await delay(500);
    return boiteText.match(/\d+/)[0] * 1;
  }
  static async getCurrentBoiteNumberOld(){
    await pad.dpad.setX(1);
    await delay(3000);
    await pad.dpad.setX(0);
    await pad.dpad.setY(1);
    await delay(3000);
    await pad.dpad.setY(-1);
    await delay(100);
    await pad.dpad.setY(0);
    await pad.dpad.setX(0);
    await delay(1000);
    const boiteText = await VIDEO.getText(735, 100,380,65, 120, false));
    await pad.dpad.setY(-1);
    await delay(3000);
    await pad.dpad.setY(1);
    await delay(100);
    await pad.dpad.setY(0);
    await delay(1000);
    await pad.dpad.setX(-1);
    await delay(3000);
    await pad.dpad.setX(1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(1000);
    return boiteText.match(/\d+/)[0] * 1;
  }  
  static async goBoite1Pos1(){
    await pad.dpad.setX(1);
    await delay(3000);
    await pad.dpad.setX(0);
    await pad.dpad.setY(1);
    await delay(3000);
    await pad.dpad.setY(-1);
    await delay(100);
    await pad.dpad.setY(0);
    await pad.dpad.setX(0);
    await delay(1000);
    while(!(await VIDEO.isSubFrame('521a87a9dbfa91ac8276efa0196e57d84637a354'))){
      await pad.RT.pressAndRelease(); 
      await delay(2000);
    }
    await pad.dpad.setY(-1);
    await delay(3000);
    await pad.dpad.setY(1);
    await delay(100);
    await pad.dpad.setY(0);
    await delay(1000);
    await pad.dpad.setX(-1);
    await delay(3000);
    await pad.dpad.setX(1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(1000);
  }
  static async goBoiteXPosX(boite: number, position: number, currentBoite?: number, currentPos?: number){
    const currentBoiteNumber = currentBoite === undefined ? (await PKMN.getCurrentBoiteNumberOld()) : currentBoite;
    const currentPosNumber = currentPos === undefined ? 1 : currentPos ;
    const direction = boite > currentBoiteNumber ? +1 : -1;
    for(let i = currentBoiteNumber ; i != boite; i += direction){
      if(direction === 1){
        await pad.RT.pressAndRelease(); 
      } else {
        await pad.LT.pressAndRelease(); 
      }
      await delay(1000);
    }
    const currentXPos = (currentPosNumber-1)%6;
    const currentYPos = Math.floor((currentPosNumber  - 1)/6);
    const xPos = (position-1)%6;
    const yPos = Math.floor((position  - 1)/6);    
    const xDiff = xPos - currentXPos;
    const yDiff = yPos - currentYPos;
    const xDir = xDiff > 0 ? +1 : -1;
    const yDir = yDiff > 0 ? +1 : -1;
 

    for(let i = 0; i < Math.abs(xDiff); i++){
      await pad.dpad.setX(xDir);
      await delay(100);
      await pad.dpad.setX(0);
      await delay(1000);
    }
    for(let i = 0; i < Math.abs(yDiff); i++){
      await pad.dpad.setY(yDir);
      await delay(100);
      await pad.dpad.setY(0);
      await delay(1000);
    }    
  }
  
  static async isTalentCache(dexNumber: number, talent: string){
    if(dexNumber ===  1) { 
        return talent.indexOf('Créa-Herbe') !== -1;
    }
    if(dexNumber ===  2) { 
        return talent.indexOf('Créa-Herbe') !== -1;
    }
    if(dexNumber ===  3) { 
        return talent.indexOf('Créa-Herbe') !== -1;
    }
    if(dexNumber ===  4) { 
        return talent.indexOf('Libéro') !== -1;
    }
    if(dexNumber ===  5) { 
        return talent.indexOf('Libéro') !== -1;
    }
    if(dexNumber ===  6) { 
        return talent.indexOf('Libéro') !== -1;
    }
    if(dexNumber ===  7) { 
        return talent.indexOf('Sniper') !== -1;
    }
    if(dexNumber ===  8) { 
        return talent.indexOf('Sniper') !== -1;
    }
    if(dexNumber ===  9) { 
        return talent.indexOf('Sniper') !== -1;
    }
    if(dexNumber ===  10) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  11) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  12) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  13) { 
        return talent.indexOf('Fuite') !== -1;
    }
    if(dexNumber ===  14) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  15) { 
        return talent.indexOf('Lentiteintée') !== -1;
    }
    if(dexNumber ===  16) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  17) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  18) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  19) { 
        return talent.indexOf('Lentiteintée') !== -1;
    }
    if(dexNumber ===  20) { 
        return talent.indexOf('Lentiteintée') !== -1;
    }
    if(dexNumber ===  21) { 
        return talent.indexOf('Coeur de Coq') !== -1;
    }
    if(dexNumber ===  22) { 
        return talent.indexOf('Coeur de Coq') !== -1;
    }
    if(dexNumber ===  23) { 
        return talent.indexOf('Armure Miroir') !== -1;
    }
    if(dexNumber ===  24) { 
        return talent.indexOf('Gloutonnerie') !== -1;
    }
    if(dexNumber ===  25) { 
        return talent.indexOf('Gloutonnerie') !== -1;
    }
    if(dexNumber ===  26) { 
        return talent.indexOf('Rivalité') !== -1;
    }
    if(dexNumber ===  27) { 
        return talent.indexOf('Rivalité') !== -1;
    }
    if(dexNumber ===  28) { 
        return talent.indexOf('Rivalité') !== -1;
    }
    if(dexNumber ===  29) { 
        return talent.indexOf('Filature') !== -1;
    }
    if(dexNumber ===  30) { 
        return talent.indexOf('Filature') !== -1;
    }
    if(dexNumber ===  31) { 
        return talent.indexOf('Pied Véloce') !== -1;
    }
    if(dexNumber ===  32) { 
        return talent.indexOf('Pied Véloce') !== -1;
    }
    if(dexNumber ===  33) { 
        return talent.indexOf('Acharné') !== -1;
    }
    if(dexNumber ===  34) { 
        return talent.indexOf('Pare-Balles') !== -1;
    }
    if(dexNumber ===  35) { 
        return talent.indexOf('Pare-Balles') !== -1;
    }
    if(dexNumber ===  36) { 
        return talent.indexOf('Tempo Perso') !== -1;
    }
    if(dexNumber ===  37) { 
        return talent.indexOf('Tempo Perso') !== -1;
    }
    if(dexNumber ===  38) { 
        return talent.indexOf('Tempo Perso') !== -1;
    }
    if(dexNumber ===  39) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  40) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  41) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  42) { 
        return talent.indexOf('Glissade') !== -1;
    }
    if(dexNumber ===  43) { 
        return talent.indexOf('Glissade') !== -1;
    }
    if(dexNumber ===  44) { 
        return talent.indexOf('Farceur') !== -1;
    }
    if(dexNumber ===  45) { 
        return talent.indexOf('Farceur') !== -1;
    }
    if(dexNumber ===  46) { 
        return talent.indexOf('Phobique') !== -1;
    }
    if(dexNumber ===  47) { 
        return talent.indexOf('Battant') !== -1;
    }
    if(dexNumber ===  48) { 
        return talent.indexOf('Coloforce') !== -1;
    }
    if(dexNumber ===  49) { 
        return talent.indexOf('Coloforce') !== -1;
    }
    if(dexNumber ===  50) { 
        return talent.indexOf('Multi-Coups') !== -1;
    }
    if(dexNumber ===  51) { 
        return talent.indexOf('Multi-Coups') !== -1;
    }
    if(dexNumber ===  52) { 
        return talent.indexOf('Gluco-Voile') !== -1;
    }
    if(dexNumber ===  53) { 
        return talent.indexOf('Gluco-Voile') !== -1;
    }
    if(dexNumber ===  54) { 
        return talent.indexOf('Gluco-Voile') !== -1;
    }
    if(dexNumber ===  55) { 
        return talent.indexOf('Fuite') !== -1;
    }
    if(dexNumber ===  56) { 
        return talent.indexOf('Puanteur') !== -1;
    }
    if(dexNumber ===  57) { 
        return talent.indexOf('Pose Spore') !== -1;
    }
    if(dexNumber ===  58) { 
        return talent.indexOf('Coeur Soin') !== -1;
    }
    if(dexNumber ===  59) { 
        return talent.indexOf('Feuille Garde') !== -1;
    }
    if(dexNumber ===  60) { 
        return talent.indexOf('Feuille Garde') !== -1;
    }
    if(dexNumber ===  61) { 
        return talent.indexOf('Technicien') !== -1;
    }
    if(dexNumber ===  62) { 
        return talent.indexOf('Cuvette') !== -1;
    }
    if(dexNumber ===  63) { 
        return talent.indexOf('Cuvette') !== -1;
    }
    if(dexNumber ===  64) { 
        return talent.indexOf('Essaim') !== -1;
    }
    if(dexNumber ===  65) { 
        return talent.indexOf('Essaim') !== -1;
    }
    if(dexNumber ===  66) { 
        return talent.indexOf('Moins') !== -1;
    }
    if(dexNumber ===  67) { 
        return talent.indexOf('Moins') !== -1;
    }
    if(dexNumber ===  68) { 
        return talent.indexOf('Sécheresse') !== -1;
    }
    if(dexNumber ===  69) { 
        return talent.indexOf('Sécheresse') !== -1;
    }
    if(dexNumber ===  70) { 
        return talent.indexOf('Coeur Noble') !== -1;
    }
    if(dexNumber ===  71) { 
        return talent.indexOf('Coeur Noble') !== -1;
    }
    if(dexNumber ===  72) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  73) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  74) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  75) { 
        return talent.indexOf('Isograisse') !== -1;
    }
    if(dexNumber ===  76) { 
        return talent.indexOf('Isograisse') !== -1;
    }
    if(dexNumber ===  77) { 
        return talent.indexOf('Isograisse') !== -1;
    }
    if(dexNumber ===  78) { 
        return talent.indexOf('Insomnia') !== -1;
    }
    if(dexNumber ===  79) { 
        return talent.indexOf('Lunatique') !== -1;
    }
    if(dexNumber ===  80) { 
        return talent.indexOf('Lunatique') !== -1;
    }
    if(dexNumber ===  81) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  82) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  83) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  84) { 
        return talent.indexOf('Attention') !== -1;
    }
    if(dexNumber ===  85) { 
        return talent.indexOf('Attention') !== -1;
    }
    if(dexNumber ===  86) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  87) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  88) { 
        return talent.indexOf('Annule Garde') !== -1;
    }
    if(dexNumber ===  89) { 
        return talent.indexOf('Annule Garde') !== -1;
    }
    if(dexNumber ===  90) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  91) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  92) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  93) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  94) { 
        return talent.indexOf('Joli Sourire') !== -1;
    }
    if(dexNumber ===  95) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  96) { 
        return talent.indexOf('Anti-Bruit') !== -1;
    }
    if(dexNumber ===  97) { 
        return talent.indexOf('Anti-Bruit') !== -1;
    }
    if(dexNumber ===  98) { 
        return talent.indexOf('Sans Limite') !== -1;
    }
    if(dexNumber ===  99) { 
        return talent.indexOf('Sans Limite') !== -1;
    }
    if(dexNumber ===  100) { 
        return talent.indexOf('Inconscient') !== -1;
    }
    if(dexNumber ===  101) { 
        return talent.indexOf('Inconscient') !== -1;
    }
    if(dexNumber ===  102) { 
        return talent.indexOf('Adaptabilité') !== -1;
    }
    if(dexNumber ===  103) { 
        return talent.indexOf('Adaptabilité') !== -1;
    }
    if(dexNumber ===  104) { 
        return talent.indexOf('Fuite') !== -1;
    }
    if(dexNumber ===  105) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  106) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  107) { 
        return talent.indexOf('Esprit Vital') !== -1;
    }
    if(dexNumber ===  108) { 
        return talent.indexOf('Délestage') !== -1;
    }
    if(dexNumber ===  109) { 
        return talent.indexOf('Attention') !== -1;
    }
    if(dexNumber ===  110) { 
        return talent.indexOf('Impassible') !== -1;
    }
    if(dexNumber ===  111) { 
        return talent.indexOf('Querelleur') !== -1;
    }
    if(dexNumber ===  112) { 
        return talent.indexOf('Querelleur') !== -1;
    }
    if(dexNumber ===  113) { 
        return talent.indexOf('Corps Sain') !== -1;
    }
    if(dexNumber ===  114) { 
        return talent.indexOf('Corps Sain') !== -1;
    }
    if(dexNumber ===  115) { 
        return talent.indexOf('Corps Sain') !== -1;
    }
    if(dexNumber ===  116) { 
        return talent.indexOf('Agitation') !== -1;
    }
    if(dexNumber ===  117) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  118) { 
        return talent.indexOf('Heavy Metal') !== -1;
    }
    if(dexNumber ===  119) { 
        return talent.indexOf('Heavy Metal') !== -1;
    }
    if(dexNumber ===  120) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  121) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  122) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  123) { 
        return talent.indexOf('Coeur Noble') !== -1;
    }
    if(dexNumber ===  124) { 
        return talent.indexOf('Rage Brûlure') !== -1;
    }
    if(dexNumber ===  125) { 
        return talent.indexOf('Rage Brûlure') !== -1;
    }
    if(dexNumber ===  126) { 
        return talent.indexOf('Pose Spore') !== -1;
    }
    if(dexNumber ===  127) { 
        return talent.indexOf('Pose Spore') !== -1;
    }
    if(dexNumber ===  128) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  129) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  130) { 
        return talent.indexOf('Regard Vif') !== -1;
    }
    if(dexNumber ===  131) { 
        return talent.indexOf('Regard Vif') !== -1;
    }
    if(dexNumber ===  132) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  133) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  134) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  135) { 
        return talent.indexOf('Fouille') !== -1;
    }
    if(dexNumber ===  136) { 
        return talent.indexOf('Fouille') !== -1;
    }
    if(dexNumber ===  137) { 
        return talent.indexOf('Fouille') !== -1;
    }
    if(dexNumber ===  138) { 
        return talent.indexOf('Impassible') !== -1;
    }
    if(dexNumber ===  139) { 
        return talent.indexOf('Impassible') !== -1;
    }
    if(dexNumber ===  140) { 
        return talent.indexOf('Impassible') !== -1;
    }
    if(dexNumber ===  141) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  142) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  143) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  144) { 
        return talent.indexOf('Phobique') !== -1;
    }
    if(dexNumber ===  145) { 
        return talent.indexOf('Impudence') !== -1;
    }
    if(dexNumber ===  146) { 
        return talent.indexOf('Paratonnerre') !== -1;
    }
    if(dexNumber ===  147) { 
        return talent.indexOf('Paratonnerre') !== -1;
    }
    if(dexNumber ===  148) { 
        return talent.indexOf('Lunatique') !== -1;
    }
    if(dexNumber ===  149) { 
        return talent.indexOf('Lunatique') !== -1;
    }
    if(dexNumber ===  150) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  151) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  152) { 
        return talent.indexOf('Adaptabilité') !== -1;
    }
    if(dexNumber ===  153) { 
        return talent.indexOf('Joli Sourire') !== -1;
    }
    if(dexNumber ===  154) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  155) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  156) { 
        return talent.indexOf('Inconscient') !== -1;
    }
    if(dexNumber ===  157) { 
        return talent.indexOf('Boom Final') !== -1;
    }
    if(dexNumber ===  158) { 
        return talent.indexOf('Boom Final') !== -1;
    }
    if(dexNumber ===  159) { 
        return talent.indexOf('Corps Ardent') !== -1;
    }
    if(dexNumber ===  160) { 
        return talent.indexOf('Corps Ardent') !== -1;
    }
    if(dexNumber ===  161) { 
        return talent.indexOf('Torche') !== -1;
    }
    if(dexNumber ===  162) { 
        return talent.indexOf('Torche') !== -1;
    }
    if(dexNumber ===  163) { 
        return talent.indexOf('Torche') !== -1;
    }
    if(dexNumber ===  164) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  165) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  166) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  167) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  168) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  169) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  170) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  171) { 
        return talent.indexOf('Poing de Fer') !== -1;
    }
    if(dexNumber ===  172) { 
        return talent.indexOf('Poing de Fer') !== -1;
    }
    if(dexNumber ===  173) { 
        return talent.indexOf('Poing de Fer') !== -1;
    }
    if(dexNumber ===  174) { 
        return talent.indexOf('Simple') !== -1;
    }
    if(dexNumber ===  175) { 
        return talent.indexOf('Simple') !== -1;
    }
    if(dexNumber ===  176) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  177) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  178) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  179) { 
        return talent.indexOf('Sans Limite') !== -1;
    }
    if(dexNumber ===  180) { 
        return talent.indexOf('Propulseur') !== -1;
    }
    if(dexNumber ===  181) { 
        return talent.indexOf('Propulseur') !== -1;
    }
    if(dexNumber ===  182) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  183) { 
        return talent.indexOf('Boost Acier') !== -1;
    }
    if(dexNumber ===  184) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  185) { 
        return talent.indexOf('Aroma-Voile') !== -1;
    }
    if(dexNumber ===  186) { 
        return talent.indexOf('Aroma-Voile') !== -1;
    }
    if(dexNumber ===  187) { 
        return talent.indexOf('Gluco-Voile') !== -1;
    }
    if(dexNumber ===  188) { 
        return talent.indexOf('Gluco-Voile') !== -1;
    }
    if(dexNumber ===  189) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  190) { 
        return talent.indexOf('Anticipation') !== -1;
    }
    if(dexNumber ===  191) { 
        return talent.indexOf('Insomnia') !== -1;
    }
    if(dexNumber ===  192) { 
        return talent.indexOf('Insomnia') !== -1;
    }
    if(dexNumber ===  193) { 
        return talent.indexOf('Paratonnerre') !== -1;
    }
    if(dexNumber ===  194) { 
        return talent.indexOf('Paratonnerre') !== -1;
    }
    if(dexNumber ===  195) { 
        return talent.indexOf('Paratonnerre') !== -1;
    }
    if(dexNumber ===  196) { 
        return talent.indexOf('Anticipation') !== -1;
    }
    if(dexNumber ===  197) { 
        return talent.indexOf('Hydratation') !== -1;
    }
    if(dexNumber ===  198) { 
        return talent.indexOf('Pied Véloce') !== -1;
    }
    if(dexNumber ===  199) { 
        return talent.indexOf('Cran') !== -1;
    }
    if(dexNumber ===  200) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  201) { 
        return talent.indexOf('Attention') !== -1;
    }
    if(dexNumber ===  202) { 
        return talent.indexOf('Chlorophylle') !== -1;
    }
    if(dexNumber ===  203) { 
        return talent.indexOf('Corps Gel') !== -1;
    }
    if(dexNumber ===  204) { 
        return talent.indexOf('Peau Féérique') !== -1;
    }
    if(dexNumber ===  205) { 
        return talent.indexOf('Pare-Balles') !== -1;
    }
    if(dexNumber ===  206) { 
        return talent.indexOf('Agitation') !== -1;
    }
    if(dexNumber ===  207) { 
        return talent.indexOf('Isograisse') !== -1;
    }
    if(dexNumber ===  208) { 
        return talent.indexOf('Tempo Perso') !== -1;
    }
    if(dexNumber ===  209) { 
        return (talent.indexOf('Farceur') !== -1) || (talent.indexOf('Battant') !== -1);
    }
    if(dexNumber ===  210) { 
        return talent.indexOf('Délestage') !== -1;
    }
    if(dexNumber ===  211) { 
        return talent.indexOf('Délestage') !== -1;
    }
    if(dexNumber ===  212) { 
        return talent.indexOf('Aroma-Voile') !== -1;
    }
    if(dexNumber ===  213) { 
        return talent.indexOf('Aroma-Voile') !== -1;
    }
    if(dexNumber ===  214) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  215) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  216) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  217) { 
        return talent.indexOf('Télépathe') !== -1;
    }
    if(dexNumber ===  218) { 
        return talent.indexOf('Querelleur') !== -1;
    }
    if(dexNumber ===  219) { 
        return talent.indexOf('Querelleur') !== -1;
    }
    if(dexNumber ===  220) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  221) { 
        return talent.indexOf('Absorbe-Eau') !== -1;
    }
    if(dexNumber ===  222) { 
        return talent.indexOf('Toxitouche') !== -1;
    }
    if(dexNumber ===  223) { 
        return talent.indexOf('Toxitouche') !== -1;
    }
    if(dexNumber ===  224) { 
        return talent.indexOf('Intimidation') !== -1;
    }
    if(dexNumber ===  225) { 
        return talent.indexOf('Intimidation') !== -1;
    }
    if(dexNumber ===  226) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  227) { 
        return talent.indexOf('Contestation') !== -1;
    }
    if(dexNumber ===  228) { 
        return talent.indexOf('Hydratation') !== -1;
    }
    if(dexNumber ===  229) { 
        return talent.indexOf('Hydratation') !== -1;
    }
    if(dexNumber ===  230) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  231) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  232) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  233) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  234) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  235) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  236) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  237) { 
        return talent.indexOf('Corps Condamné') !== -1;
    }
    if(dexNumber ===  238) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  239) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  240) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  241) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  242) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  243) { 
        return talent.indexOf('Miroir Magik') !== -1;
    }
    if(dexNumber ===  244) { 
        return talent.indexOf('Benêt') !== -1;
    }
    if(dexNumber ===  245) { 
        return talent.indexOf('Benêt') !== -1;
    }
    if(dexNumber ===  246) { 
        return talent.indexOf('Pression') !== -1;
    }
    if(dexNumber ===  247) { 
        return talent.indexOf('Pression') !== -1;
    }
    if(dexNumber ===  248) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  249) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  250) { 
        return talent.indexOf('Puanteur') !== -1;
    }
    if(dexNumber ===  251) { 
        return talent.indexOf('Créa-Brume') !== -1;
    }
    if(dexNumber ===  252) { 
        return talent.indexOf('Phobique') !== -1;
    }
    if(dexNumber ===  253) { 
        return talent.indexOf('Phobique') !== -1;
    }
    if(dexNumber ===  254) { 
        return talent.indexOf('Garde-Ami') !== -1;
    }
    if(dexNumber ===  255) { 
        return talent.indexOf('Garde-Ami') !== -1;
    }
    if(dexNumber ===  256) { 
        return talent.indexOf('Inconscient') !== -1;
    }
    if(dexNumber ===  257) { 
        return talent.indexOf('Chanceux') !== -1;
    }
    if(dexNumber ===  258) { 
        return talent.indexOf('Chanceux') !== -1;
    }
    if(dexNumber ===  259) { 
        return talent.indexOf('Chanceux') !== -1;
    }
    if(dexNumber ===  260) { 
        return talent.indexOf('Gloutonnerie') !== -1;
    }
    if(dexNumber ===  261) { 
        return talent.indexOf('Gloutonnerie') !== -1;
    }
    if(dexNumber ===  262) { 
        return talent.indexOf('Chlorophylle') !== -1;
    }
    if(dexNumber ===  263) { 
        return talent.indexOf('Chlorophylle') !== -1;
    }
    if(dexNumber ===  264) { 
        return talent.indexOf('Téméraire') !== -1;
    }
    if(dexNumber ===  265) { 
        return talent.indexOf('Téméraire') !== -1;
    }
    if(dexNumber ===  266) { 
        return talent.indexOf('Téméraire') !== -1;
    }
    if(dexNumber ===  267) { 
        return talent.indexOf('Marque Ombre') !== -1;
    }
    if(dexNumber ===  268) { 
        return talent.indexOf('Marque Ombre') !== -1;
    }
    if(dexNumber ===  269) { 
        return talent.indexOf('Marque Ombre') !== -1;
    }
    if(dexNumber ===  270) { 
        return talent.indexOf('Régé-Force') !== -1;
    }
    if(dexNumber ===  271) { 
        return talent.indexOf('Régé-Force') !== -1;
    }
    if(dexNumber ===  272) { 
        return talent.indexOf('Régé-Force') !== -1;
    }
    if(dexNumber ===  273) { 
        return talent.indexOf('Annule Garde') !== -1;
    }
    if(dexNumber ===  274) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  275) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  276) { 
        return talent.indexOf('Délestage') !== -1;
    }
    if(dexNumber ===  277) { 
        return talent.indexOf('Analyste') !== -1;
    }
    if(dexNumber ===  278) { 
        return talent.indexOf('Analyste') !== -1;
    }
    if(dexNumber ===  279) { 
        return talent.indexOf('Glissade') !== -1;
    }
    if(dexNumber ===  280) { 
        return talent.indexOf('Glissade') !== -1;
    }
    if(dexNumber ===  281) { 
        return talent.indexOf('Agitation') !== -1;
    }
    if(dexNumber ===  282) { 
        return talent.indexOf('Acharné') !== -1;
    }
    if(dexNumber ===  283) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  284) { 
        return talent.indexOf('Armurouillée') !== -1;
    }
    if(dexNumber ===  285) { 
        return talent.indexOf('Regard Vif') !== -1;
    }
    if(dexNumber ===  286) { 
        return talent.indexOf('Regard Vif') !== -1;
    }
    if(dexNumber ===  287) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  288) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  289) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  290) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  291) { 
        return talent.indexOf('Infiltration') !== -1;
    }
    if(dexNumber ===  292) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  293) { 
        return talent.indexOf('Pickpocket') !== -1;
    }
    if(dexNumber ===  294) { 
        return talent.indexOf('Farceur') !== -1;
    }
    if(dexNumber ===  295) { 
        return talent.indexOf('Sans Limite') !== -1;
    }
    if(dexNumber ===  296) { 
        return talent.indexOf('Lavabo') !== -1;
    }
    if(dexNumber ===  297) { 
        return talent.indexOf('Lentiteintée') !== -1;
    }
    if(dexNumber ===  298) { 
        return talent.indexOf('Farceur') !== -1;
    }
    if(dexNumber ===  299) { 
        return talent.indexOf('Coeur Noble') !== -1;
    }
    if(dexNumber ===  300) { 
        return talent.indexOf('Coque Armure') !== -1;
    }
    if(dexNumber ===  301) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  302) { 
        return talent.indexOf('Heavy Metal') !== -1;
    }
    if(dexNumber ===  303) { 
        return talent.indexOf('Heavy Metal') !== -1;
    }
    if(dexNumber ===  304) { 
        return talent.indexOf('Intimidation') !== -1;
    }
    if(dexNumber ===  305) { 
        return talent.indexOf('Moiteur') !== -1;
    }
    if(dexNumber ===  306) { 
        return talent.indexOf('Moiteur') !== -1;
    }
    if(dexNumber ===  307) { 
        return talent.indexOf('Régé-Force') !== -1;
    }
    if(dexNumber ===  308) { 
        return talent.indexOf('Régé-Force') !== -1;
    }
    if(dexNumber ===  309) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  310) { 
        return talent.indexOf('Maladresse') !== -1;
    }
    if(dexNumber ===  311) { 
        return talent.indexOf('Technicien') !== -1;
    }
    if(dexNumber ===  312) { 
        return talent.indexOf('Voile Sable') !== -1;
    }
    if(dexNumber ===  313) { 
        return talent.indexOf('Voile Sable') !== -1;
    }
    if(dexNumber ===  314) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  315) { 
        return talent.indexOf('Force Sable') !== -1;
    }
    if(dexNumber ===  316) { 
        return talent.indexOf('Absentéisme') !== -1;
    }
    if(dexNumber ===  317) { 
        return talent.indexOf('Écran Fumée') !== -1;
    }
    if(dexNumber ===  318) { 
        return talent.indexOf('Force Soleil') !== -1;
    }
    if(dexNumber ===  319) { 
        return talent.indexOf('Force Soleil') !== -1;
    }
    if(dexNumber ===  320) { 
        return talent.indexOf('Brise Moule') !== -1;
    }
    if(dexNumber ===  321) { 
        return talent.indexOf('Sans Limite') !== -1;
    }
    if(dexNumber ===  322) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  323) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  324) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  325) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  326) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  327) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  328) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  329) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  330) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  331) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  332) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  333) { 
        return talent.indexOf('Anticipation') !== -1;
    }
    if(dexNumber ===  334) { 
        return talent.indexOf('Anticipation') !== -1;
    }
    if(dexNumber ===  335) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  336) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  337) { 
        return talent.indexOf('Créa-Psy') !== -1;
    }
    if(dexNumber ===  338) { 
        return talent.indexOf('Récolte') !== -1;
    }
    if(dexNumber ===  339) { 
        return talent.indexOf('Récolte') !== -1;
    }
    if(dexNumber ===  340) { 
        return talent.indexOf('Cuvette') !== -1;
    }
    if(dexNumber ===  341) { 
        return talent.indexOf('Cuvette') !== -1;
    }
    if(dexNumber ===  342) { 
        return talent.indexOf('Symbiose') !== -1;
    }
    if(dexNumber ===  343) { 
        return talent.indexOf('Acharné') !== -1;
    }
    if(dexNumber ===  344) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  345) { 
        return talent.indexOf('Acharné') !== -1;
    }
    if(dexNumber ===  346) { 
        return talent.indexOf('Ciel Gris') !== -1;
    }
    if(dexNumber ===  347) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  348) { 
        return talent.indexOf('Fermeté') !== -1;
    }
    if(dexNumber ===  349) { 
        return talent.indexOf('Écailles Glacées') !== -1;
    }
    if(dexNumber ===  350) { 
        return talent.indexOf('Écailles Glacées') !== -1;
    }
    if(dexNumber ===  351) { 
        return talent.indexOf('Technicien') !== -1;
    }
    if(dexNumber ===  352) { 
        return talent.indexOf('Technicien') !== -1;
    }
    if(dexNumber ===  353) { 
        return talent.indexOf('Créa-Élec') !== -1;
    }
    if(dexNumber ===  354) { 
        return talent.indexOf('Ignifu-Voile') !== -1;
    }
    if(dexNumber ===  355) { 
        return talent.indexOf('Ignifu-Voile') !== -1;
    }
    if(dexNumber ===  356) { 
        return talent.indexOf('Pression') !== -1;
    }
    if(dexNumber ===  357) { 
        return talent.indexOf('Pression') !== -1;
    }
    if(dexNumber ===  358) { 
        return talent.indexOf('Fermeté') !== -1;
    }
    if(dexNumber ===  359) { 
        return talent.indexOf('Fermeté') !== -1;
    }
    if(dexNumber ===  360) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  361) { 
        return talent.indexOf('Hydratation') !== -1;
    }
    if(dexNumber ===  362) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  363) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  364) { 
        return talent.indexOf('Technicien') !== -1;
    }
    if(dexNumber ===  365) { 
        return (talent.indexOf('Corps Gel') !== -1) || (talent.indexOf('Technicie') !== -1);
    }
    if(dexNumber ===  366) { 
        return talent.indexOf('Corps Gel') !== -1;
    }
    if(dexNumber ===  367) { 
        return talent.indexOf('Attention') !== -1;
    }
    if(dexNumber ===  368) { 
        return talent.indexOf('Mode Transe') !== -1;
    }
    if(dexNumber ===  369) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  370) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  371) { 
        return talent.indexOf('Nerfs') !== -1;
    }
    if(dexNumber ===  372) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  373) { 
        return talent.indexOf('Imposteur') !== -1;
    }
    if(dexNumber ===  374) { 
        return talent.indexOf('Baigne Sable') !== -1;
    }
    if(dexNumber ===  375) { 
        return talent.indexOf('Chasse-Neige') !== -1;
    }
    if(dexNumber ===  376) { 
        return talent.indexOf('Baigne Sable') !== -1;
    }
    if(dexNumber ===  377) { 
        return talent.indexOf('Chasse-Neige') !== -1;
    }
    if(dexNumber ===  378) { 
        return talent.indexOf('Force Soleil') !== -1;
    }
    if(dexNumber ===  379) { 
        return talent.indexOf('Force Soleil') !== -1;
    }
    if(dexNumber ===  380) { 
        return talent.indexOf('Force Soleil') !== -1;
    }
    if(dexNumber ===  381) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  382) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  383) { 
        return talent.indexOf('Voile Sable') !== -1;
    }
    if(dexNumber ===  384) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  385) { 
        return talent.indexOf('Tension') !== -1;
    }
    if(dexNumber ===  386) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  387) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  388) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  389) { 
        return talent.indexOf('Poisseux') !== -1;
    }
    if(dexNumber ===  390) { 
        return talent.indexOf('Poisseux') !== -1;
    }
    if(dexNumber ===  391) { 
        return talent.indexOf('Poisseux') !== -1;
    }
    if(dexNumber ===  392) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  393) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  394) { 
        return talent.indexOf('Envelocape') !== -1;
    }
    if(dexNumber ===  395) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  396) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  397) { 
        return talent.indexOf('Corps Maudit') !== -1;
    }
    if(dexNumber ===  398) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  399) { 
        return talent.indexOf('-') !== -1;
    }
    if(dexNumber ===  400) { 
        return talent.indexOf('-') !== -1;
}
}
  static async goToBoitePc(){
    await pad.X.pressAndRelease(); 
    await delay(2000); 
    await pad.dpad.setX(-1);
    await delay(3000);
    await pad.dpad.setX(0);
    await pad.dpad.setY(-1);
    await delay(3000);
    await pad.dpad.setY(0);
    await pad.dpad.setX(1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(1000);
    await pad.A.pressAndRelease();
    await delay(2000);
    await pad.RT.pressAndRelease(); 
    await delay(2000);
  }
  static async leaveBoitePc(){
    for(let i = 0; i < 20; i++){
      await pad.B.pressAndRelease();
      await delay(500);
      if(await PKMN.canDoCommY()){
        await delay(1000);
        return;
      }
    }
  }
  static async getStatistics(){
    let talent;
    let watchDog = 0;
    do {
      await pad.PLUS.pressAndRelease(); 
      await delay(600);    
      talent = await VIDEO.getText(1340, 610, 420, 45, 160, false);
      if(watchDog++ > 6){
        return null;
      }
    } while (talent.indexOf('Talent') === -1)
    talent = talent.replace(/Talent/, '').trim();
    await pad.PLUS.pressAndRelease(); 
    await delay(600);
    
    await pad.A.pressAndRelease(); 
    await delay(500);
    let dexNumberText = (await VIDEO.getText(1330, 100, 150, 50, 160, false));
    let dexNumber =  dexNumberText.match(/\d+/) ? (dexNumberText.match(/\d+/)[0] * 1) : -1;
    let name = (await VIDEO.getText(1485, 100, 300, 55, 160, false)).trim();
    let shiny = !(await VIDEO.isSubFrame('3b939b8c7fd766d897d87782b2bd9e171ba5b08a'));
    await pad.B.pressAndRelease(); 
    await delay(500);
        
    let ivs = (await VIDEO.getText(1485, 215, 300, 330, 160, false))
      .replace(/\n\n/g,"\n")
      .split("\n");

    let pv = ivs[0];
    let attaque = ivs[1];
    let defense = ivs[2];
    let attaqueSpe = ivs[3];
    let defenseSpe = ivs[4];
    let vitesse = ivs[5];
    let countIvs = ivs.filter((iv) => {
      return iv.match(/xception/)
    }).length;
    let perfect = (countIvs === 6);
    let isTalentCache = await PKMN.isTalentCache(dexNumber, talent);
    let isInteressant = (
      countIvs == 6 || shiny || isTalentCache ||
      (dexNumber === 373 && countIvs > 4)
    );
    if(dexNumber === 144){ // magicarpe
      // 30 sec => 1 280 pas
      isInteressant = (shiny)
    }
    if(dexNumber === 378){ // salameche
      isInteressant = (shiny)
    }    
    if(dexNumber === 395){ // fantyrm
      // 6min => 10 240 pas 
      isInteressant = (shiny)
    }   
    if(name === 'Ramoloss' || name === 'Bulbizarre' || name === 'Carapuce'){
      isInteressant = (shiny)
    }
    if(name === 'Carapuce'){
      isInteressant = (shiny) && 
      pv.match(/xception/) && 
      defense.match(/xception/) && 
      attaqueSpe.match(/xception/) && 
      defenseSpe.match(/xception/) && 
      vitesse.match(/xception/) && 
      talent.match(/uvett/)
    }    
    if(dexNumber === 307){ // vorasterie
      // 6min => 10 240 pas 
      isInteressant = (shiny)
    }            
    let stats = {
      dexNumber,
      name,
      shiny,
      talent, 
      countIvs,
      perfect,
      isInteressant,
      pv, 
      attaque, 
      attaqueSpe,
      defense,
      defenseSpe,
      vitesse
    };
    
    
    return stats;

  }
  static async getStatsOfPoke(boite: number, position: number){
    await PKMN.goToBoitePc();
    await PKMN.goBoiteXPosX(boite, position);
    const stats = await PKMN.getStatistics();
    await PKMN.leaveBoitePc();
    return stats;
  }
  static async getStatsOfPokeAtPos(position: number = 6){
    await PKMN.goToBoitePc();
    await pad.dpad.setX(-1);
    await delay(100);
    await pad.dpad.setX(0);
    await delay(1000);
    for(let i = 1; i < position; i++){
        await pad.dpad.setY(1);
        await delay(100);
        await pad.dpad.setY(0);
        await delay(1000);
    }
    const stats = await PKMN.getStatistics();
    await PKMN.leaveBoitePc();
    return stats;
  }
  static async autoRangerPoke(
       startBoite: number; 
       startPos: number; 
       endBoite: number;
       endPos: number;
      ){
      LOG.log('Starting auto rangement');
      let currentBoite = startBoite; 
      let currentPos = startPos;
      let maxScoreAccepted = endBoite*30 + endPos;
      await PKMN.goToBoitePc();
      while(true){
        const currentScore = currentBoite*30 + currentPos;
        if(currentScore > maxScoreAccepted) {
            await PKMN.leaveBoitePc();
          return;
        }
        await PKMN.goBoiteXPosX(currentBoite, currentPos);
        const stats = await PKMN.getStatistics();
        if(stats === null){
          currentPos++; 
          if(currentPos > 30){
            currentPos = 1;
            currentBoite++;
          }
          continue;
        }
        const pos = (stats.dexNumber - 1) % 30;
        const boi = Math.floor((stats.dexNumber - pos) / 30);
        pos++; 
        boi++;
        await pad.A.pressAndRelease(); 
        await delay(500);
        await pad.A.pressAndRelease(); 
        await delay(500);    
        LOG.log(JSON.stringify({context: ' RANGER ', stats, boi,pos}));
        await PKMN.goBoiteXPosX(boi, pos);
        await pad.A.pressAndRelease(); 
        await delay(500);
        currentPos++; 
        if(currentPos > 30){
          currentPos = 1;
          currentBoite++;
        }
      }
    }
  static async autoTradeAndRange(
       startBoite: number; 
       startPos: number; 
       endBoite: number;
       endPos: number;    
    ){
      LOG.log('Starting auto trade rangement');
      let currentBoite = startBoite; 
      let currentPos = startPos;
      let maxScoreAccepted = endBoite*30 + endPos;
      while(true){
        const currentScore = currentBoite*30 + currentPos;
        if(currentScore > maxScoreAccepted) {
          return;
        }
        const stats = await PKMN.getStatsOfPoke(currentBoite, currentPos); 
        if(stats === null || (stats !== null && (stats.isInteressant))){
          currentPos++; 
          if(currentPos > 30){
            currentPos = 1;
            currentBoite++;
          }  
          continue;
        }
        await PKMN.autoMagicTrade(currentBoite, currentPos, currentBoite, currentPos);
        await PKMN.autoRangerPoke(currentBoite, currentPos, currentBoite, currentPos);
      }      
    }
  static async autoJetePokeBidon(
       startBoite: number; 
       startPos: number; 
       endBoite: number;
       endPos: number; 
  ){
      LOG.log('Starting auto jetage');
      let currentBoite = startBoite; 
      let currentPos = startPos;
      let prevBoite = undefined;
      let prevPos = undefined;
      let maxScoreAccepted = endBoite*30 + endPos;
      await PKMN.goToBoitePc();
      while(true){
        const currentScore = currentBoite*30 + currentPos;
        if(currentScore > maxScoreAccepted) {
          await PKMN.leaveBoitePc();
          return;
        }
        await PKMN.goBoiteXPosX(currentBoite, currentPos, prevBoite, prevPos);
        const stats = await PKMN.getStatistics();
        console.log(stats); 
        prevBoite = currentBoite; 
        prevPos = currentPos;
        if(stats === null || stats.isInteressant){
        } else {
          await pad.A.pressAndRelease(); 
          await delay(500);
          for(let i = 0; i < 2; i++){
            await pad.dpad.setY(-1);
            await delay(100);
            await pad.dpad.setY(0);
            await delay(1000);         
          }
          await pad.A.pressAndRelease(); 
          await delay(1000);          
          await pad.dpad.setY(-1);
          await delay(100);
          await pad.dpad.setY(0);
          await delay(1000);
          for(let i = 0; i < 3; i++){
            await pad.A.pressAndRelease(); 
            await delay(500);   
          }
          await delay(500);
        }
        currentPos++; 
        if(currentPos > 30){
          currentPos = 1;
          currentBoite++;
        }             

      }       
    
  }

  // Resets 
  static async resetSalamecheTarak(){
    while(true){
      await SWITCH.resetGame();
      await pad.A.pressAndRelease();
      await delay(2000);
      for(let i = 0; i < 4; i++){
        await pad.A.pressAndRelease();
        await delay(3000);
      }
      for(let i = 0; i < 8; i++){
        await pad.B.pressAndRelease();
        await delay(3000);
      }
      await PKMN.goToBoitePc();
      await PKMN.goBoiteXPosX(1,16);
      const stats = await PKMN.getStatistics();
      LOG.log(stats);
      if(stats.perfect === true || stats.shiny === true){
        return;
      }
    }
  }
  
  static async fullAutoTrain(
    startParentBoite: number, startParentPos: number, 
    endParentBoite: number, endParentPos: number, 
    startBabiesBoite: number, startBabiesPos: number,
    endBabiesBoite: number, endBabiesPos: number,
    numbersOfBabiesPerRound: number = 25
  ){
    let currentParentBoite = startParentBoite; 
    let currentParentPos = startParentPos;
    while(true){
      // Chaning the poke of breed 
      await PKMN.flyBackToPensionGirl();
      await PKMN.getStatsOfPoke(currentParentBoite,currentParentPos);
      await PKMN.speakToPensionGirlAndRecuperePoke();
      await PKMN.speakToPensionGirlAndGivePoke(currentParentBoite,currentParentPos);
      
      // Breeding the poke 
      await PKMN.getStatsOfPoke(startBabiesBoite,startBabiesPos);
      await PKMN.autoDoBabies(numbersOfBabiesPerRound, false);
      await delay(3000);
      await PKMN.autoJetePokeBidon(
        startBabiesBoite, startBabiesPos, 
        endBabiesBoite, endBabiesPos
      );
      await delay(2000); 
      for(let i=0; i<9; i++){ await pad.B.pressAndRelease(); await delay(900) };
      await delay(2000); 
      
      
      currentParentPos++; 
      if(currentParentPos > 30){
        currentParentPos = 1;
        currentParentBoite++;
      }
      let currentParentScore = currentParentBoite * 30 + currentParentPos; 
      let endParentScore = endParentBoite * 30 + endParentPos;
      if(currentParentScore > endParentScore){
        currentParentBoite = startParentBoite; 
        currentParentPos = startParentPos;
      }
      
    }
  }
}


async function isInCombat(){
  LOG.log('isInCombat')
  const textToFind = 'Fuir';
  return (await VIDEO.getText(1580,980,100,60)).indexOf(textToFind) !== -1;
}
async function isCombatFini(){
    LOG.log('isCombatFini')

  const textToFind = 'Votre équipe a gagné des Points d’Expérience';
  return (await VIDEO.getText(140,870,1200,100)).indexOf(textToFind) !== -1;
}
async function isInFinDeCombat(){
  LOG.log('isInFinDeCombat')
  const textFound = await VIDEO.getText(120,27,300,55));
  LOG.log('inInFinDeCombat ? ', textFound);
  return textFound.indexOf('lambino') !== -1 || 
          textFound.indexOf('apyro') !== -1 ||
          textFound.indexOf('pyrobut') !== -1;
}
async function finirFinDeCombat(){
  LOG.log('finirFinDeCombat')
  while(await isInFinDeCombat()){
    for(let i = 0; i < 5; i++){
      await pad.A.pressAndRelease();
      await delay(200);
    }
  }
  await delay(1000);
  for(let i = 0; i < 50; i++){
    await pad.B.pressAndRelease();
    await delay(200);
  }
  await delay(1000);
}
async function goToHerbe() {
  LOG.log('goToHerbe')
  await pad.leftStick.setY(255);
  await delay(2000)
  await pad.leftStick.reset();
  await pad.leftStick.setX(0);
  await delay(2000);
  await pad.leftStick.reset();
  await pad.leftStick.setY(255);
  await delay(3500);
  await pad.leftStick.reset();
  await pad.leftStick.setX(0);
  await delay(1400);
  await pad.leftStick.reset();
  await pad.leftStick.setY(255);
  await delay(6000)
  await pad.leftStick.reset();
  await pad.leftStick.setX(250);
  await delay(1500)
  await pad.leftStick.reset();
}
async function goToPokeCenter() {
  LOG.log('goToPokeCenter')
await pad.leftStick.setX(0);
await delay(1500)
await pad.leftStick.reset();
await pad.leftStick.setY(0);
await delay(5700)
await pad.leftStick.reset();
await pad.leftStick.setX(250);
await delay(1300);
await pad.leftStick.reset();
await pad.leftStick.setY(0);
await delay(3300);
await pad.leftStick.reset();
await pad.leftStick.setX(250);
await delay(2000);
await pad.leftStick.reset();
await pad.leftStick.setY(0);
await delay(2000)
await pad.leftStick.reset();
await delay(4000);
}
async function healPokemon(){
  LOG.log('healPokemon')
  await pad.leftStick.setY(0);
  await delay(1000)
  await pad.leftStick.reset();
  for(let i = 0; i < 30; i++){
    await pad.A.pressAndRelease();
  }
  await delay(5000);
  for(let i = 0; i < 30; i++){
    await pad.B.pressAndRelease();
  }  
  await delay(2000)
}
async function leavePokeCenter(){
  LOG.log('leavePokeCenter')
  await pad.leftStick.setY(250);
  await delay(1200);
  await pad.leftStick.reset();
  await delay(3000); 
}
async function combat() {
  LOG.log('combat')
  while(true){
    if(await isCombatFini()){
      await finirFinDeCombat();
      break;
    }    
    for(let i = 0; i < 2; i++){
      await pad.A.pressAndRelease();
      await delay(1000);
    }
    await delay(3000);
  }
}
async function waitForCombat(){
  LOG.log('waitForCombat')
  while(!(await isInCombat())){
    await delay(5000);
    if(!(await isInCombat())){
      await pad.A.pressAndRelease();  
    }
    await delay(1000);
  }
}

// console.log((await PKMN.pensionGirlIsProposingEgg())); return;
// await pad.HOME.pressAndRelease(); return;
// await SWITCH.pairPadAndBack2Game(); await pad.A.pressAndRelease(); return; 

while(true){
  await PKMN.autoJetePokeBidon(20,1,20,30);
  await PKMN.autoDoBabies(25, false);
}

 

//await PKMN.autoJetePokeBidon(20,1,20,30);
for(let i=0; i<9; i++){ await pad.B.pressAndRelease(); await delay(900) }; 
await delay(2000);
await PKMN.autoJetePokeBidon(20,1,20,30);
await delay(2000); 
await PKMN.fullAutoTrain(
  19,1, 
  19,5,
  20,1,
  20,30,
  25 // numbersOfBabiesPerRound
)
return;




















